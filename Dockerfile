FROM node:12-alpine

RUN mkdir -p /usr/src/app && chown -R node:node /usr/src/app

WORKDIR /usr/src/app

COPY --chown=node:node . .

USER node

RUN npm install

EXPOSE 8080

CMD ["npm", "start"]