# BAH MTS Okta API

## REST API for BAH MTS' customized Okta dashboard.

### Prerequisites:
- NodeJS version >= 12 e.g. 12.3.1
- .env file in the project root to define port, Okta API base URL, and API Key. Sample file:

```
PORT=8000
OKTA_BASE_URL=https://dev-508049.okta.com/api/v1
OKTA_API_KEY=app_api_key

AUTH_REDIRECT=http://localhost:4200/rotating
AUTH_LOGIN=http://localhost:4200/login
CORS_ORIGIN=http://localhost:4200

REDIS_HOST=localhost
REDIS_PORT=6379
REDIS_SESSION_TTL=3600

OKTA_IDP_BASE_URL=https://ssodev.mts.bah.com
OKTA_CLIENT_ID=okta_app_client_id
OKTA_CLIENT_SECRET=okta_app_client_secret
APP_BASE_URL=http://localhost:8080
```

### To run the server from the project root:
```
npm i
npm run webpack
```
### In a new tab:
```
npm start
```

### Deploying to AWS
```
docker build -t bah-mts-rest .
```
Make sure that awscli is installed and your AWS credentials are configured:

`aws configure`

To push the latest Docker image:

```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 009178328312.dkr.ecr.us-east-1.amazonaws.com/bah-mts

docker tag bah-mts-rest:latest 009178328312.dkr.ecr.us-east-1.amazonaws.com/bah-mts

docker push 009178328312.dkr.ecr.us-east-1.amazonaws.com/bah-mts:latest
```

To force the ECS cluster to grab the latest:

```
aws ecs update-service --cluster bah-mts --service bah-mts-rest --force-new-deployment
```
