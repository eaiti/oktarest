import axios from 'axios';
import dotenv from 'dotenv';
import { Application } from "../model/application";
import { logger } from "./logging.service";

export default class ApplicationService {
    OKTA_BASE_URL: string;
    AXIOS_CONFIG: any;
    AXIOS_BAD_CONFIG: any;

    constructor() {
        dotenv.config();
        this.OKTA_BASE_URL = process.env.OKTA_BASE_URL || "localhost:8080";
        this.AXIOS_CONFIG = {
            headers: { 'Content-Type': 'application/json', 'Authorization': `SSWS ${process.env.OKTA_API_KEY}` }
        };
    }

    async getApplications(userId: string): Promise<Application[]> {
        try {
            logger.info(`Sending request to ${this.OKTA_BASE_URL}/apps?filter=user.id+eq+\"${userId}\"`)
            const res = await axios.get(`${this.OKTA_BASE_URL}/apps?filter=user.id+eq+\"${userId}\"`, this.AXIOS_CONFIG);
            const apps = (res.data as Application[])
                .filter(a => a.id !== process.env.OKTA_CLIENT_ID && a.status == 'ACTIVE');

            return apps;
        } catch (e) {
            logger.error(`Error while retrieving Okta Applications ${e}`);
            return [];
        }
    }
}