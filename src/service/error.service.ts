import { Request, Response, NextFunction } from "express";
import { logger } from "./logging.service"

export function errorHandling(
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
) {
  logger.info(`Error: ${err.status}`);
  switch(err?.response?.status) {
    case 400:
      res.status(400).json({status: 400, message: 'Bad Request'});
      logger.error("Request returned with 400 error");
      break;
    case 401:
      res.status(401).json({status: 401, message: 'User is not authenticated'});
      logger.error("Request returned with 401 error");
      break;
    case 403:
      res.status(403).json({status: 401, message: 'Forbidden'});
      logger.error("Request returned with 403 error");
      break;
    case 404:
      res.status(404).json({status: 404, message: 'Resource not found'});
      logger.error("Request returned with 404 error");
      break;
    default:
      res.status(500);
      res.json({status: 500, message: 'Application Error'});
      logger.error(`Application Error: ${JSON.stringify(err?.response?.data)}`)
    }
}
