import axios from 'axios';
import dotenv from 'dotenv';
import { Application } from "../model/application";
import { Role } from '../model/role';
import { logger } from './logging.service';

export default class AuthService {

    OKTA_BASE_URL: string;
    AXIOS_CONFIG: any;

    constructor() {
        dotenv.config();
        this.OKTA_BASE_URL = process.env.OKTA_BASE_URL || "localhost:8080";
        this.AXIOS_CONFIG = {
            headers: { 'Content-Type': 'application/json', 'Authorization': `SSWS ${process.env.OKTA_API_KEY}` }
        };
    }

    async getRoles(userId: string): Promise<Role[]> {
        try {
            logger.info(`Sending request to ${this.OKTA_BASE_URL}/users/${userId}/roles`);
            const res = await axios.get(`${this.OKTA_BASE_URL}/users/${userId}/roles`, this.AXIOS_CONFIG);
            return res.data as Role[];
        } catch (e) {
            logger.error(`Error while retrieving user Okta Roles ${e}`);
            return [];
        }
    }
}