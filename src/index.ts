import * as dotenv from "dotenv";
import express from "express";
import { Response } from "express";
import cors from "cors";
import helmet from "helmet";
import session from "express-session";
import redis from "redis";
import connectRedis from 'connect-redis';
import { ExpressOIDC } from "@okta/oidc-middleware";

import { applicationRouter } from "./controller/application.controller";

import { authRouter } from "./controller/auth.controller";
import { Server } from "http";

import { errorHandling } from "./service/error.service";
import { logger } from "./service/logging.service"


dotenv.config();

if (!process.env.PORT) {
    process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);

const app = express();
app.use(helmet());

/* set up cors */
var corsOptions = {
    origin: `${process.env.CORS_ORIGIN}`,
    credentials: true,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors(corsOptions));

app.use(express.json());

/* set up Redis as the session store */
const REDIS_PORT = parseInt(process.env.REDIS_PORT as string, 10);
const redisClient = redis.createClient({
    host: `${process.env.REDIS_HOST}`,
    port: REDIS_PORT
});
const redisStore = connectRedis(session);

const REDIS_SESSION_TTL = parseInt(process.env.REDIS_SESSION_TTL as string, 10);
logger.info(`REDIS HOST: ${process.env.REDIS_HOST}`);
app.use(session({
    secret: `${process.env.OKTA_CLIENT_SECRET}`,
    resave: true,
    saveUninitialized: false,
    store: new redisStore({ host: `${process.env.REDIS_HOST}`, port: REDIS_PORT, client: redisClient, ttl: REDIS_SESSION_TTL }),
}));

/* set up OIDC middleware */
const oidc = new ExpressOIDC({
    issuer: `${process.env.OKTA_IDP_BASE_URL}/oauth2/default`,
    appBaseUrl: `${process.env.APP_BASE_URL}`,
    client_id: `${process.env.OKTA_CLIENT_ID}`,
    client_secret: `${process.env.OKTA_CLIENT_SECRET}`,
    redirect_uri: `${process.env.APP_BASE_URL}/authorization-code/callback`,
    scope: 'openid profile'
});

app.use(oidc.router);

app.get('/', (req: any, res: Response) => {
    if (req.userContext) {
        res.redirect(`${process.env.AUTH_REDIRECT}`);
    } else {
        res.redirect(`${process.env.AUTH_LOGIN}`);
    }
});

app.get('/health', (req: any, res: Response) => {
    res.send({ status: 'OK', port: PORT });
});

app.use("/application", applicationRouter);
app.use("/auth", authRouter);

app.use((req, res) => {
    logger.error(`Resource not found: ${req.url}`);
    res.status(404).json({status: 404, message: 'Resource not found'})
});

app.use(errorHandling);

let server = {} as Server;
oidc.on('ready', () => {
    server = app.listen(PORT, () => {
        logger.info(`Listening on port ${PORT}`);
    });
});

oidc.on('error', (err: any) => {
    logger.error(`Error during OIDC flow ${err}`);
});

/**
 * Webpack HMR Activation
 */
type ModuleId = string | number;

interface WebpackHotModule {
    hot?: {
        data: any;
        accept(
            dependencies: string[],
            callback?: (updatedDependencies: ModuleId[]) => void,
        ): void;
        accept(dependency: string, callback?: () => void): void;
        accept(errHandler?: (err: Error) => void): void;
        dispose(callback: (data: any) => void): void;
    };
}

declare const module: WebpackHotModule;

if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => server.close());
}