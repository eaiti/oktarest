import express, { Request, Response, NextFunction } from "express";
import ApplicationService from "../service/application.service";

const applicationService = new ApplicationService();

export const applicationRouter = express.Router();

applicationRouter.get("/", async (req: any, res: Response, next: NextFunction) => {
  try {
    const userId = req.userContext?.userinfo.sub;
    const apps = await applicationService.getApplications(userId);
    res.status(200).send(apps);
  } catch (e) {
    next(e)
  }
});