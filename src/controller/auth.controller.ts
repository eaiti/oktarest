import express, { Response } from "express";
import { User } from "../model/user";
import AuthService from "../service/auth.service";
import { logger } from "../service/logging.service";

const authService = new AuthService();

export const authRouter = express.Router();

authRouter.get("/me", async (req: any, res: Response) => {
    if (req.isAuthenticated() && req.userContext) {
        const loginDate = new Date(0);
        loginDate.setUTCSeconds(req.userContext.tokens.expires_at - parseInt(process.env.REDIS_SESSION_TTL as string, 10));
        const user: User = {
            userId: req.userContext.userinfo.sub,
            firstName: req.userContext.userinfo.given_name,
            lastName: req.userContext.userinfo.family_name,
            username: req.userContext.userinfo.preferred_username,
            loginDate
        };
        logger.info(`Authenticated user: ${JSON.stringify(user)}`);
        res.send(user);
    } else {
        logger.error(`No active session`);
        res.status(404).send('No active session');
    }
});

authRouter.get("/roles", async (req:any, res:Response) => {
    try {
        if (req.isAuthenticated() && req.userContext) {
            const userId = req.userContext?.userinfo.sub;
            const roles = await authService.getRoles(userId);
            res.status(200).send(roles);
        } else {
            res.status(404).send('No active session');
        }
    } catch (e) {
        logger.error(`Error while getting roles for user`);
    }

});