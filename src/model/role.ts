export interface Role {
    id: string;
    label: string;
    type: string;
    status: string;
    assignmentType: string;
}