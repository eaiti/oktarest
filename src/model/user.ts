export interface User {
    userId: string;
    firstName: string;
    lastName: string;
    username: string;
    loginDate: Date;
}